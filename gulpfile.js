const gulp = require('gulp');
const zip = require('gulp-zip');

function bundle() {
	return gulp.src([
		'**/*',
		'!node_modules/**',
		'!bundled/**',
		'!src/**',
		'!gulpfile.js',
		'!package.json',
		'!package-lock.json',
		'!webpack.config.js'
	])
		.pipe(zip('udemy-blocks.zip'))
		.pipe(gulp.dest('bundled'));
}

exports.bundle = bundle;