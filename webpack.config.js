const autoprefixer = require('autoprefixer');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const globImporter = require('node-sass-glob-importer');

module.exports = (env, argv) => {
	function isDevelopment() {
		return argv.mode === 'development';
	}

	let config = {
		entry: {
			editor: './src/editor.js',
			script: './src/script.js',
			scriptRtl: './src/style-rtl.js'
		},
		output: {
			filename: '[name].js'
		},
		optimization: {
			minimizer: [
				new TerserPlugin({
					sourceMap: true
				}),
				new OptimizeCSSAssetsPlugin(
					{
						cssProcessorOptions: {
							map: {
								inline: false,
								annotation: true
							}
						}
					})
			]
		},
		plugins: [
			new CleanPlugin(),
			new MiniCSSExtractPlugin({
				chunkFilename: '[id].css',
				filename: (chunkData) => {
					let name = '[name].css';

					switch (chunkData.chunk.name) {
						case 'script':
							name = 'style.css';
							break;
						case 'scriptRtl':
							name = 'style-rtl.css';
							break;
					}

					return name;
				}
			})
		],
		devtool: isDevelopment() ? 'cheap-module-source-map' : 'source-map',
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: [
								'@babel/preset-env',
								[
									'@babel/preset-react',
									{
										"pragma": "wp.element.createElement",
										"pragmaFrag": "wp.element.Fragment",
										"development": isDevelopment()
									}
								]
							]
						}
					}
				},
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						MiniCSSExtractPlugin.loader,
						'css-loader',
						{
							loader: 'postcss-loader',
							options: {
								plugins: [
									autoprefixer()
								]
							}
						},
						{
							loader: 'sass-loader',
							options: {
								importer: globImporter()
							}
						}
					]
				},
				{
					test: /\.svg$/,
					use: ['@svgr/webpack'], // Settings are in .svgrrc
				},
				{
					test: /\.(png|jpe?g|gif)$/i,
					use: [
						{
							loader: 'file-loader',
						},
					],
				},
			]
		},
		externals: {
			jquery: 'jQuery',
			'@wordpress/blocks': ['wp', 'blocks'],
			'@wordpress/i18n': ['wp', 'i18n'],
			'@wordpress/components': ['wp', 'components'],
			'@wordpress/editor': ['wp', 'editor'],
			'@wordpress/block-editor': ['wp', 'blockEditor']
		}
	};
	return config;
}
;