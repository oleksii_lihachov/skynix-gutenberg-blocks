/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {Component} from '@wordpress/element';
import {InspectorControls} from '@wordpress/block-editor';
import {BaseControl, Button, PanelBody, SelectControl} from '@wordpress/components';

/**
 * Internal dependencies
 */
import {I18N} from "../../constans";
import ItemSettings from "./ItemSettings";

/**
 * External dependencies
 */
import {cloneDeep} from "lodash";

/**
 * Create an Inspector Controls wrapper Component
 */
export default class Inspector extends Component {
	render() {
		const {attributes, setAttributes} = this.props;
		const {type, items} = attributes;

		return (
			<InspectorControls>
				<PanelBody title={__('General', I18N)} initialOpen={true}>
					<SelectControl
						label={__('Type', I18N)}
						value={type}
						options={[
							{label: __('Arrow', I18N), value: 'arrow'},
							{label: __('Check', I18N), value: 'check'},
							{label: __('Star', I18N), value: 'star'},
							{label: __('Bullet', I18N), value: 'bullet'},
						]}
						onChange={(value) => {
							setAttributes({type: value});
						}}
					/>
				</PanelBody>
				<PanelBody title={__('Items', I18N)} initialOpen={false}>
					<ItemSettings
						items={items}
						setAttributes={setAttributes}
						propName='items'
					/>
					<BaseControl className='text-center'>
						<Button isPrimary onClick={() => {
							const itemsClone = cloneDeep(items);
							itemsClone.push({text: __('Sample Item', I18N)});
							setAttributes({items: itemsClone})
						}}>
							{__('Add item', I18N)}
						</Button>
					</BaseControl>
				</PanelBody>
			</InspectorControls>
		);
	}
}