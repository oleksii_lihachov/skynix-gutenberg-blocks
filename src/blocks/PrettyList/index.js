/**
 * BLOCK: Pretty list.
 */

/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import {I18N} from "../../constans";
import {ListIcon} from "../../icons";
import edit from './edit';
import schema from "./schema";

const blockProperty = {
	title: __('Pretty List', I18N),
	description: __('List with a rich settings of styles', I18N),
	icon: ListIcon,
	category: 'skynix-modules',
	keywords: [
		'skynix',
		'list',
		'pretty'
	],
	supports: {
		customClassName: false,
		html: false,
	},
	example: {},
	attributes: schema,
	save: () => null,
	edit
};


export default {
	slug: `skynix/pretty-list`,
	blockProperty,
};