/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {I18N} from "../../constans";

const schema = {
	type: {
		type: 'string',
		default: 'arrow'
	},
	items: {
		type: 'array',
		default: [
			{
				text: __('Sample Item #1', I18N)
			},
			{
				text: __('Sample Item #2', I18N)
			},
			{
				text: __('Sample Item #3', I18N)
			}
		]
	}
};
export default schema;