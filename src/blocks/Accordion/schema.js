/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import {I18N} from "../../constans";

const schema = {
	tabs: {
		type: 'array',
		default: [
			{
				title: __('Sample title', I18N),
				content: __('Sample content', I18N)
			}
		]
	}
};

export default schema;