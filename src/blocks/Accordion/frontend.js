/**
 * WordPress dependencies
 */
import domReady from '@wordpress/dom-ready';

domReady(() => {
	if (window.SKData && window.SKData.isAdmin) {
		return;
	}

	const accordions = document.querySelectorAll('.c-accordion');

	if (accordions.length) {
		for (let i = 0; i < accordions.length; i++) {
			const accordion = accordions[i];

			accordion.addEventListener('click', function (ev) {
				const targetClass = ev.target.className;

				if (targetClass.indexOf('c-accordion-item__title') >= 0) {
					const items = accordion.getElementsByClassName('c-accordion-item');
					const currentItemClass = ev.target.parentNode.className;

					for (let j = 0; j < items.length; j++) {
						items[j].className = 'c-accordion-item close';
					}

					if (currentItemClass === 'c-accordion-item close') {
						ev.target.parentNode.className = 'c-accordion-item open';
						ev.target.nextElementSibling.classList.add('stuckMoveDownOpacity');
					}
				}
			}, false);
		}
	}
});