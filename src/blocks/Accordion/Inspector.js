/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {Component} from '@wordpress/element';
import {InspectorControls} from '@wordpress/block-editor';
import {PanelBody, BaseControl, Button} from '@wordpress/components';

/**
 * Internal dependencies
 */
import CardList from '../../components/card-list';
import {I18N} from "../../constans";

/**
 * External dependencies
 */
import {cloneDeep} from 'lodash';

/**
 * Create an Inspector Controls wrapper Component
 */
export default class Inspector extends Component {
	render() {
		const {setAttributes, attributes} = this.props;
		const {tabs} = attributes;
		return (
			<InspectorControls>
				<PanelBody title={__('Tabs', I18N)} initialOpen>
					<CardList
						items={tabs}
						propName='tabs'
						setAttributes={setAttributes}
						titlePlaceholder={__('Sample title', I18N)}
						contentPlaceholder={__('Sample content', I18N)}
						includeContentField
					/>
					<BaseControl className='sk-advanced-range-control text-center'>
						<Button isSecondary onClick={() => {
							const tabsClone = cloneDeep(tabs);
							tabsClone.push({
								title: __('Sample title', I18N),
								content: __('Sample content', I18N)
							});

							setAttributes({
								tabs: tabsClone
							});
						}}>
							{__('Add Accordition', I18N)}
						</Button>
					</BaseControl>
				</PanelBody>
			</InspectorControls>
		);
	}
};