/**
 * BLOCK: Accordion.
 */

/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import {AccordionIcon} from "../../icons";
import schema from "./schema";
import edit from "./edit";
import {I18N} from "../../constans";

const blockProperty = {
	title: __('Accordion', I18N),
	description: __('Accordions are useful when you want to toggle between hiding and showing large amount of content', I18N),
	icon: AccordionIcon,
	category: 'skynix-modules',
	keywords: [
		'accordion',
		'skynix',
		'tabs',
		'collapsed',
		'expand'
	],
	supports: {
		customClassName: false,
		html: false,
	},
	example: {},
	attributes: schema,
	save: () => null,
	edit
};


export default {
	slug: `skynix/accordion`,
	blockProperty,
};

