import PrettyList from './PrettyList';
import Heading from './Heading';
import Accordion from './Accordion';

const Blocks = {
	PrettyList,
	Heading,
	Accordion
};

export default Blocks;