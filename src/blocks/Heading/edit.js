/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {RichText} from "@wordpress/block-editor";
import {Component, Fragment} from '@wordpress/element';
import {compose} from "@wordpress/compose";
import {withFocusOutside} from "@wordpress/components";

/**
 * Internal dependencies
 */
import {I18N} from "../../constans";
import Controls from "./Controls";
import Inspector from "./Inspector";

/**
 * External dependencies
 */
import classnames from "classnames";

class EditBlock extends Component {
	render() {
		const {
			isSelected,
			setAttributes,
			className,
			attributes: {
				content,
				level,
				backgroundText,
				textAlign,
			},
		} = this.props;
		const tagName = 'h' + level;


		const numberClasses = {
			center: 'text-center',
			left: 'text-left',
			right: 'text-right',
		};

		const mainClasses = classnames([
			className,
			'heading-number',
			`heading-number--${textAlign}`,
			numberClasses[textAlign]
		]);

		return (
			<Fragment>
				{isSelected && <Fragment>
					<Inspector {...this.props} />
					<Controls {...this.props} />
				</Fragment>}
				<div className={mainClasses}>
					<div className="heading-number__bg">{backgroundText}</div>
					<div className="heading-number__context">
						<RichText
							className='heading-number__content'
							tagName={tagName}
							placeholder={__('Write heading…', I18N)}
							value={content}
							onChange={(content) => setAttributes({content})}
							multiline={false}
							allowedFormats={[]}
							keepPlaceholderOnFocus={true}
							unstableOnSplit={() => false}
						/>
					</div>
				</div>
			</Fragment>
		);
	}
}

export default compose(
	withFocusOutside
)(EditBlock);


