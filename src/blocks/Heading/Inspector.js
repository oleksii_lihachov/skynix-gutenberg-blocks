/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {InspectorControls} from "@wordpress/block-editor";
import {Component} from '@wordpress/element';
import {PanelBody, TextControl, BaseControl} from "@wordpress/components";

/**
 * Internal dependencies
 */
import {I18N} from "../../constans";
import HeadingToolbar from "./heading-toolbar";

export default class Inspector extends Component {
	render() {
		const {
			attributes: {
				level,
				backgroundText,
			},
			setAttributes
		} = this.props;

		return (
			<InspectorControls>
				<PanelBody initialOpen={true} title={__('Main Settings', I18N)}>
					<BaseControl label={__('Level', I18N)}>
						<HeadingToolbar
							isCollapsed={false}
							minLevel={1}
							maxLevel={7}
							selectedLevel={level}
							onChange={(level) => setAttributes({level})}
						/>
					</BaseControl>
					<TextControl
						label={__('Background Text', I18N)}
						value={backgroundText}
						onChange={(backgroundText) => setAttributes({backgroundText})}
					/>
				</PanelBody>
			</InspectorControls>
		);
	}
};