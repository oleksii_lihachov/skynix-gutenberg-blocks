/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {AlignmentToolbar, BlockControls, BlockAlignmentToolbar} from "@wordpress/block-editor";
import {Component} from '@wordpress/element';

/**
 * Internal dependencies
 */
import HeadingToolbar from "./heading-toolbar";
import {I18N} from "../../constans";

const ALIGNMENT_CONTROLS = [
	{
		icon: 'editor-alignleft',
		title: __('Align Text Left', I18N),
		align: 'left',
	},
	{
		icon: 'editor-aligncenter',
		title: __('Align Text Center', I18N),
		align: 'center',
	},
	{
		icon: 'editor-alignright',
		title: __('Align Text Right', I18N),
		align: 'right',
	}
];

export default class Controls extends Component {
	render() {
		const {
			attributes: {
				blockAlignment,
				textAlign,
				level
			},
			setAttributes
		} = this.props;

		return (
			<BlockControls>
				<BlockAlignmentToolbar
					value={blockAlignment}
					onChange={blockAlignment => setAttributes({blockAlignment})}
					controls={['wide', 'full']}
				/>
				<AlignmentToolbar
					value={textAlign}
					onChange={textAlign => setAttributes({textAlign})}
					alignmentControls={ALIGNMENT_CONTROLS}
				/>
				<HeadingToolbar
					isCollapsed={false}
					minLevel={2}
					maxLevel={6}
					selectedLevel={level}
					onChange={(level) => setAttributes({level})}
				/>
			</BlockControls>
		);
	}
}