/**
 * BLOCK: Heading.
 */

/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';

/**
 * Internal dependencies
 */
import {I18N} from "../../constans";
import edit from './edit';
import schema from "./schema";

const blockProperty = {
	title: __('Heading', I18N),
	description: __('Heading with number in background', I18N),
	icon: {
		src: 'editor-spellcheck',
	},
	category: 'skynix-modules',
	keywords: [
		'skynix',
		'heading'
	],
	supports: {
		customClassName: false,
		html: false,
		align: ['wide', 'full']
	},
	example: {},
	attributes: schema,
	save: () => null,
	edit
};


export default {
	slug: `skynix/heading`,
	blockProperty,
};