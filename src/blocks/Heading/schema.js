/**
 * WordPress dependencies
 */
import {__} from '@wordpress/i18n';
import {I18N} from "../../constans";

const schema = {
	textAlign: {
		type: 'string',
		default: 'left',
	},
	level: {
		type: "number",
		default: 2,
	},
	content: {
		type: 'string',
		default: __('Heading', I18N),
	},
	backgroundText: {
		type: 'string',
		default: __('01.', I18N),
	}
};
export default schema;