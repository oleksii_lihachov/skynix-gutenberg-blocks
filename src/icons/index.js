import SVGlistIcon from './list-icon.svg';
import SVGAccordionIcon from './accordion-icon.svg';

export const ListIcon = () => {
	return <SVGlistIcon width="20" height="20"/>
};

export const AccordionIcon = () => {
	return <SVGAccordionIcon width="20" height="20"/>
};