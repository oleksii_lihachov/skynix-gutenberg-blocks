<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 *
 * @wordpress-plugin
 * Plugin Name:       Skynix Gutenberg Blocks
 * Plugin URI:        https://bitbucket.org/oleksii_lihachov/skynix-gutenberg-blocks/src
 * Description:       Skynix Gutenberg Bcloks.
 * Version:           1.0.0
 * Author:            Skynix
 * Author URI:        http://skynix.co
 * Text Domain:       sk-blocks
 * Domain Path:       /languages
 */

namespace SK_Guten;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// ======================  CONSTANTS ===================================
define( 'SK_VERSION', '1.0.0' );
define( 'SK_PLUGIN_SLUG', 'skynix-gutenberg-blocks' );
define( 'SK_TEXT_DOMAIN', 'sk-blocks' );
define( 'SK_PLUGIN_DIR', plugin_dir_path( dirname( __FILE__ ) ) . SK_PLUGIN_SLUG . '/' );
define( 'SK_PLUGIN_URL', plugin_dir_url( dirname( __FILE__ ) ) . SK_PLUGIN_SLUG . '/' );
define( 'SK_LOG_DIR', SK_PLUGIN_DIR . 'logs/' );

require_once plugin_dir_path( __FILE__ ) . 'autoloader.php';
$loader = new \Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace( 'SK_Guten', __DIR__ . '/inc' );
$loader->addNamespace( 'SK_Guten\Blocks', __DIR__ . '/inc' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/sk-blocks.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/sk-blocks-activation.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_sk_blocks() {
	$plugin = new SK_Blocks();
	$plugin->run();
}

try {
	run_sk_blocks();
} catch ( \Exception $e ) {
	if ( is_writable( SK_LOG_DIR ) ) {
		file_put_contents( SK_LOG_DIR . 'error_run.txt', $e );
	}
}

/**
 * The code that runs during plugin activation.
 */
register_activation_hook( __FILE__, function () {
	flush_rewrite_rules();
	new SK_Blocks_Activation();

	if ( is_writable( SK_LOG_DIR ) ) {
		file_put_contents( SK_LOG_DIR . 'error_activation.txt', ob_get_contents() );
	}
} );

/**
 * The code that runs during plugin deactivation.
 */
register_deactivation_hook( __FILE__, function () {
	flush_rewrite_rules();
} );