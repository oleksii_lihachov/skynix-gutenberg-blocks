<?php

namespace SK_Guten;


class Rest {
	private $rest_namespace = 'skynix/v1';

	public function action_rest_api_init_trait() {
		register_rest_route( $this->rest_namespace . '/posts',
			'/get',
			array(
				array(
					'methods'             => 'POST',
					'permission_callback' => function ( $request ) {
						return current_user_can( 'editor' ) || current_user_can( 'administrator' );
					},
					'callback'            => array( $this, 'rest_get_posts' ),
				)
			)
		);
	}

	public function rest_get_posts( $request ) {
	}
}