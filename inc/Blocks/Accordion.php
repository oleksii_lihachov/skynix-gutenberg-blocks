<?php

namespace SK_Guten\Blocks;

class Accordion extends Basic {
	protected $name = 'accordion';

	protected $attributes = array(
		'tabs' => array(
			'type'    => 'object',
			'default' => array(
				array(
					'title'   => 'Sample title',
					'content' => 'Sample content'
				)
			),
		),
	);

	protected function render( $settings = array(), $inner_content = '' ) {
		$html = '';
		$tabs = $settings['tabs'];

		if ( empty( $tabs ) ) {
			echo '';
			return null;
		}

		$html .= '<div class="c-accordion">';

		foreach ( $tabs as $tab ) {
			$html .= '<div class="c-accordion-item close">';
			$html .= '	<h3 class="c-accordion-item__title">' . esc_html( $tab['title'] ) . '</h3>';
			$html .= '	<div class="c-accordion-item__content">' . esc_html( $tab['content'] ) . '</div>';
			$html .= '</div>';
		}

		$html .= '</div>';

		echo $html;
	}
}