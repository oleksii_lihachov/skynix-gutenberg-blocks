<?php

namespace SK_Guten\Blocks;

class Heading extends Basic {
	protected $name = 'heading';

	protected $attributes = array(
		'level'          => array(
			'type'    => 'number',
			'default' => 2,
		),
		'content'        => array(
			'type'    => 'string',
			'default' => 'Heading',
		),
		'backgroundText' => array(
			'type'    => 'string',
			'default' => '01.',
		),
		'textAlign'      => array(
			'type'    => 'string',
			'default' => 'left',
		),
	);

	protected function render( $settings = array(), $inner_content = '' ) {
		$align = $settings['textAlign'];

		$html = '<div class="heading-number heading-number--' . esc_attr( $align ) . ' text-' . esc_attr( $align ) . '">';
		$html .= '	<div class="heading-number__bg">' . esc_html( $settings['backgroundText'] ) . '</div>';
		$html .= '	<div class="heading-number__context">';
		$html .= '		<h' . $settings['level'] . ' class="heading-number__content">';
		$html .= '		' . esc_html( $settings['content'] ) . ' ';
		$html .= '		</h' . $settings['level'] . '>';
		$html .= '	</div>';
		$html .= '</div>';

		echo $html;
	}
}