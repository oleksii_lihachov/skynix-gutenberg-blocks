<?php

namespace SK_Guten\Blocks;

class Basic {
	protected $name = 'basic';
	protected $attributes = array();

	public function __construct() {
		add_action( 'init', array( $this, 'init_handler' ) );
	}

	public function init_handler() {
		register_block_type( 'skynix/' . $this->name, array(
			'attributes'      => $this->attributes,
			'render_callback' => array( $this, 'render_block' ),
		) );
	}

	public function render_block( $attributes, $inner_content ) {
		ob_start();
		$content = $this->render( $attributes, $inner_content );

		return strlen( $content ) ? $content : ob_get_clean();
	}

	protected function render( $attributes, $inner_content ) {
		return '';
	}
}