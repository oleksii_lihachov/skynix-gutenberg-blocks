<?php

namespace SK_Guten;

defined( 'ABSPATH' ) OR exit;

class Init {
	public function block_categories_filter( $categories, $post ) {
		array_splice( $categories, 3, 0, array(
			array(
				'slug'  => 'skynix-modules',
				'title' => __( 'Skynix Blocks', SK_TEXT_DOMAIN ),
			)
		) );

		return $categories;
	}

	public function run() {
		new Blocks\PrettyList();
		new Blocks\Heading();
		new Blocks\Accordion();
	}
}