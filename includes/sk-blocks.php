<?php

/**
 * The file that defines the core plugin class
 *
 * This is used to define internationalization, admin-specific hooks, and public-facing site hooks.
 *
 */

namespace SK_Guten;

class SK_Blocks {
	protected $loader;
	protected $assets;
	protected $plugin_name;
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 */

	public function __construct() {
		if ( defined( 'SK_VERSION' ) ) {
			$this->version = SK_VERSION;
		} else {
			$this->version = '1.0.0';
		}

		$this->plugin_name = SK_PLUGIN_SLUG;
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/sk-blocks-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/sk-blocks-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/sk-blocks-public.php';

		$this->loader = new SK_Blocks_Loader();
		$this->assets = new SK_Blocks_Public();
		$api          = new Rest();
		$init         = new Init();

		$this->loader->add_filter( 'block_categories', $init, 'block_categories_filter', 10, 2 );
		$this->loader->add_action( 'rest_api_init', $api, 'action_rest_api_init_trait', 10, 1 );

		$init->run();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the SK_Blocks_I18n class in order to set the domain and to register the hook
	 * with WordPress.
	 */
	private function set_locale() {
		$plugin_i18n = new SK_Blocks_I18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_text_domain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 */
	private function define_admin_hooks() {
		$this->loader->add_action( 'enqueue_block_editor_assets', $this->assets, 'editor_gutenberg', 10, 0 );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 */
	private function define_public_hooks() {
		$this->loader->add_action( 'enqueue_block_assets', $this->assets, 'frontend_assets', 10, 0 );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 */
	public function run() {
		$this->loader->run();
	}
}