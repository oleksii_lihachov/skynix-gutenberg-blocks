<?php

namespace SK_Guten;

class SK_Blocks_Public {
	private $localized_data = array();

	public function __construct() {
		$this->localized_data = array(
			'pluginDirUrl' => SK_PLUGIN_URL,
			'isRtl'        => is_rtl(),
			'isAdmin'      => is_admin(),
		);
	}

	public function editor_gutenberg() {
		wp_enqueue_script(
			'sk-blocks-editor',
			SK_PLUGIN_URL . 'dist/editor.js',
			null,
			SK_VERSION,
			true
		);

		wp_localize_script( 'sk-blocks-editor', 'SKData', $this->localized_data );

		wp_enqueue_style(
			'sk-blocks-editor',
			SK_PLUGIN_URL . 'dist/editor.css',
			null,
			SK_VERSION
		);
	}

	public function frontend_assets() {
		wp_enqueue_script(
			'sk-blocks-script',
			SK_PLUGIN_URL . 'dist/script.js',
			array( 'jquery' ),
			SK_VERSION,
			true
		);

		wp_localize_script( 'sk-blocks-script', 'SKData', $this->localized_data );

		wp_enqueue_style(
			'sk-blocks-styles',
			SK_PLUGIN_URL . 'dist/style.css',
			null,
			SK_VERSION
		);
		wp_style_add_data( 'sk-blocks-styles', 'rtl', true );
	}
}