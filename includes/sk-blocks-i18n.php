<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 */

namespace SK_Guten;

class SK_Blocks_I18n {
	/**
	 * Load the plugin text domain for translation.
	 */
	public function load_plugin_text_domain() {
		load_plugin_textdomain(
			SK_TEXT_DOMAIN,
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
	}
}